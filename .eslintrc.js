module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    '@vue/standard'
  ],
  rules: {
    'no-console': 'off'
  },
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
    allowImportExportEverywhere: false,
    codeFrame: true
  }
}
