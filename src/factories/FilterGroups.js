const faker = require('faker')

const FilterGroupFactory = ({ uuid, active, label } = {}) => {
  return {
    [uuid || faker.random.uuid()]: {
      active: active || faker.random.boolean(),
      label: label || faker.lorem.words(2)
    }
  }
}

module.exports = FilterGroupFactory
